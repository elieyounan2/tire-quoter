Tire-Quoter : Accurate and official tire price quoter



DESCRIPTION:

This program is created to simplify our work at a tire shop. Before this program, employees had access to 
product prices and used to give a verbal quote to customers. There was no tool that helped them achieve an
accurate quote that included everthing needed. Using this program, employees are able to take a screenshot
of an estimated total that breaks down the transaction and hand the quote to a customer. Employees are able
to select brand/model of tires, then enter price and interact with other parameters that will change the 
outcome of the estimated total for the customer to see. The program is designed to update a total as soon as
a user (employee) changes a parameter from install, to parts included and other fees. Program also checks for
certain conditions whenever user needs to include a coupon or a sale (sale differs dependent on brand).
Program will also check for more conditions like when is a coupon or a sale is allowed. Program will not include coupon unless all conditions are set by user. Given how the parameters interact with each other, it is impossible for the user to create a false quote. For example, a user cannot activate coupon without selecting installation fee. A user cannot select certain parts/add-ons without installation. A user cannot activate coupon without the correct quantity sold etc.

SCREENSHOTS:

How the program looks, quote is split between option 1 for first product and option 2 for second.

![Screen_Shot_2023-03-20_at_7.22.56_PM](/uploads/6c0f34cefed6762fd319c46995775fd3/Screen_Shot_2023-03-20_at_7.22.56_PM.png)

Some error prompts when user selects a parameter (like coupon) when another isn't :

![Screen_Shot_2023-03-20_at_7.34.30_PM](/uploads/79b8db8f456fc0dfcba1974f5c387efc/Screen_Shot_2023-03-20_at_7.34.30_PM.png)

More conditions :

![Screen_Shot_2023-03-20_at_7.34.40_PM](/uploads/6499f331ce96bf341dc923c8324e9181/Screen_Shot_2023-03-20_at_7.34.40_PM.png)


TOOLS:

This program was made possible using Java GUI builder by IntelliJ with swing utilities and components.
Launch4J was also a tool used to convert the .jar file (program was designed on a MAC) to a .exe

SUMMARY:

This program has helped facilitate our jobs at the shop by significantly reducing the amount of time spent on calculating estimated totals. And by eliminating errors of a verbal miscommunication between the customer and an employee when talking over the price given that an official print-out is handed to a customer to bring back at a later date when they're ready for a purchase. At a click of a button, user is able to gather a total as soon as they enter price, and all the recommended extra fees are auto-populated with the imported price. 

NOTE: THIS IS NOT CREATED OR RECOGNIZED BY COSTCO WHOLESALE.