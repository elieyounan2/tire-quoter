import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;



public class TireQuoter extends JFrame {

    public JPanel Panel;
    private JComboBox brandOne;
    private JComboBox modelOne;
    private JTextField warrantyOne;
    private JComboBox modelTwo;
    private JTextField WarrantyTwo;
    private JTextField priceOne;
    private JCheckBox installOne;
    private JCheckBox tpmsOne;
    private JTextField priceTwo;
    private JCheckBox installTwo;
    private JCheckBox tpmsTwo;
    private JLabel subTotal1;
    private JLabel actualTotal1;
    private JLabel actualTotal2;
    private JComboBox brandTwo;
    private JSpinner quantityOne;
    private JSpinner quantityTwo;
    private JLabel date;
    private JLabel subTotal2;
    private JLabel couponOne;
    private JCheckBox activateCheckBox1;
    private JCheckBox activateCheckBox2;
    private JLabel couponTwo;
    private JTextField sizeOne;
    private JTextField sizeTwo;
    private JTextField expirationDateOne;
    private JTextField expirationDateTwo;
    private JTextField vehicleDescription;
    private JCheckBox disposal1;
    private JCheckBox disposal2;
    private JLabel couponLabelOne;
    private JLabel couponLabelTwo;
    private JButton printButton;


    DecimalFormat df = new DecimalFormat("#.##");



    public TireQuoter() {

        //Combo-box center alignment
        ((JLabel) brandOne.getRenderer()).setHorizontalAlignment((int) CENTER_ALIGNMENT);
        ((JLabel) brandTwo.getRenderer()).setHorizontalAlignment((int) CENTER_ALIGNMENT);
        ((JLabel) modelOne.getRenderer()).setHorizontalAlignment((int) CENTER_ALIGNMENT);
        ((JLabel) modelTwo.getRenderer()).setHorizontalAlignment((int) CENTER_ALIGNMENT);

        //Set date
        LocalDate day = LocalDate.now();
        date.setText(String.valueOf(day));

        df.setRoundingMode(RoundingMode.CEILING);

        modelOne.disable();


        brandOne.addActionListener(new ActionListener() {

            //Add brands
            @Override
            public void actionPerformed(ActionEvent e) {

                //reset parameters if user defaults to no brand
                //Allows user to start over
                if (brandOne.getSelectedItem().equals("Select Brand")) {
                    activateCheckBox1.setSelected(false);
                    couponLabelOne.setVisible(false);
                    couponOne.setText(null);
                    installOne.setSelected(false);
                    tpmsOne.setSelected(false);
                    disposal1.setSelected(false);
                    actualTotal1.setText(null);
                    subTotal1.setText(null);
                    modelOne.disable();
                    modelOne.removeAllItems();
                    modelOne.addItem("Model");
                }

                /*
                    In the event user is switching brand but has already set a price and a total.
                    Checks for checked conditions and calculates math accordingly while
                    navigating between brands.
                 */
                if (brandOne.getSelectedItem().equals("Michelin") ||
                brandOne.getSelectedItem().equals("Bridgestone")) {
                    if (activateCheckBox1.isSelected()) {
                        couponOne.setText("$150 OFF");
                        couponLabelOne.setVisible(true);
                        if (installOne.isSelected()) {
                            if (tpmsOne.isSelected()) {
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if (disposal1.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));

                            } else if (!tpmsOne.isSelected()) {
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                if (disposal1.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));
                            }

                        }
                    }
                    if(brandOne.getSelectedItem().equals("Michelin")){
                        modelOne.removeAllItems();
                        modelOne.enable();
                        modelOne.addItem("Model");
                        modelOne.addItem("Defender 2");
                        modelOne.addItem("X-Tour 2");
                        modelOne.addItem("Premier LTX");
                        modelOne.addItem("CrossClimate 2");
                        modelOne.addItem("Defender LTX");
                        modelOne.addItem("X LT A/S");
                        modelOne.addItem("LTX M/S 2");
                        modelOne.addItem("LTX A/T 2");
                        modelOne.addItem("Primacy MXM4");
                        modelOne.addItem("Primacy LTX");
                        modelOne.addItem("Primacy All Season");
                        modelOne.addItem("Primacy HP");
                        modelOne.addItem("Primacy Tour A/S");
                        modelOne.addItem("Defender T+H");
                        modelOne.addItem("Agilis CrossClimate");
                        modelOne.addItem("Latitude Tour HP");
                        modelOne.addItem("Energy Saver A/S");
                        modelOne.addItem("Pilot Sport 4S");
                        modelOne.addItem("Pilot Sport A/S 4");
                        modelOne.addItem("Pilot Sport 4 SUV");
                        modelOne.addItem("Pilot Sport 4");
                        modelOne.addItem("Pilot Sport PS2");
                        modelOne.addItem("Pilot Super Sport");
                        modelOne.addItem("Pilot Sport Cup 2");
                        modelOne.addItem("Latitude X-Ice Xi2");
                        modelOne.addItem("Pilot Alpin PA4");
                        modelOne.addItem("X-ICE Snow");
                        modelOne.addItem("X-Ice Xi3");
                        modelOne.addItem("Pilot Alpin 5");
                        modelOne.addItem("Pilot Alpin 5 SUV");
                        modelOne.addItem("Latitude Alpin");
                    }
                    else if(brandOne.getSelectedItem().equals("Bridgestone")){
                        modelOne.enable();
                        modelOne.removeAllItems();
                        modelOne.addItem("Model");
                        modelOne.addItem("Turanza QuietTrack");
                        modelOne.addItem("WeatherPeak");
                        modelOne.addItem("Ecopia EP422+");
                        modelOne.addItem("Alenza AS Ultra");
                        modelOne.addItem("Dueler H/L Alenza+");
                        modelOne.addItem("Dueler A/T Revo 3");
                        modelOne.addItem("DriveGuard+");
                        modelOne.addItem("Dueler H/T 684 II");
                        modelOne.addItem("Dueler H/T 685");
                        modelOne.addItem("Dueler H/P Sport AS");
                        modelOne.addItem("Dueler H/P Sport");
                        modelOne.addItem("Potenza RE980AS+");
                        modelOne.addItem("Potenza Sport");
                        modelOne.addItem("Potenza RE050A");
                        modelOne.addItem("Potenza S007A RFT");
                        modelOne.addItem("Potenza RE-71RS");
                        modelOne.addItem("Blizzak LM005");
                        modelOne.addItem("Blizzak WS90");
                        modelOne.addItem("Blizzak DM-V2");
                    }



                }


                if (brandOne.getSelectedItem().equals("BFG") || brandOne.getSelectedItem().equals("Firestone")) {
                    if(activateCheckBox1.isSelected()){
                        couponOne.setText("$110 OFF");
                        couponLabelOne.setVisible(true);
                        if (installOne.isSelected()) {
                            if (tpmsOne.isSelected()) {
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if (disposal1.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 110.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));

                            } else if (!tpmsOne.isSelected()) {
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                if (disposal1.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 110.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));
                            }
                        }
                    }
                    if(brandOne.getSelectedItem().equals("BFG")){
                        modelOne.enable();
                        modelOne.removeAllItems();
                        modelOne.addItem("Model");
                        modelOne.addItem("G-Force COMP-2 A/S+");
                        modelOne.addItem("Advantage Control");
                        modelOne.addItem("Radial T/A");
                        modelOne.addItem("Trail-Terrain T/A");
                        modelOne.addItem("All-Terrain T/A KO2");
                        modelOne.addItem("Mud-Terrain T/A KM3");
                        modelOne.addItem("Advantage T/A Sport LT");
                    }
                    else if(brandOne.getSelectedItem().equals("Firestone")){
                        modelOne.enable();
                        modelOne.removeAllItems();
                        modelOne.addItem("Model");
                        modelOne.addItem("Destination LE3");
                        modelOne.addItem("Destination A/T 2");
                        modelOne.addItem("Destination X/T");
                        modelOne.addItem("FireHawk AS");
                        modelOne.addItem("FireHawk Indy 500");
                        modelOne.addItem("TransForce AT 2");
                        modelOne.addItem("TransForce HT 2");
                        modelOne.addItem("TransForce CV");
                    }

                }


            }
        });

        brandTwo.addActionListener(new ActionListener() {

            //Add brands
            @Override
            public void actionPerformed(ActionEvent e) {

                //reset parameters if user defaults to no brand
                //Allows user to start over
                if (brandTwo.getSelectedItem().equals("Select Brand")) {
                    activateCheckBox2.setSelected(false);
                    couponLabelTwo.setVisible(false);
                    couponTwo.setText(null);
                    installTwo.setSelected(false);
                    tpmsTwo.setSelected(false);
                    disposal2.setSelected(false);
                    actualTotal2.setText(null);
                    subTotal2.setText(null);
                    modelTwo.disable();
                    modelTwo.removeAllItems();
                    modelTwo.addItem("Model");
                }

                /*
                    In the event user is switching brand but has already set a price and a total.
                    Checks for checked conditions and calculates math accordingly while
                    navigating between brands.
                 */
                if (brandTwo.getSelectedItem().equals("Michelin") ||
                brandTwo.getSelectedItem().equals("Bridgestone")) {
                    if (activateCheckBox2.isSelected()) {
                        couponTwo.setText("$150 OFF");
                        couponLabelTwo.setVisible(true);
                        if (installTwo.isSelected()) {
                            if (tpmsTwo.isSelected()) {
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if (disposal2.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));

                            } else if (!tpmsTwo.isSelected()) {
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                if (disposal2.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));
                            }

                        }
                    }
                    if(brandTwo.getSelectedItem().equals("Michelin")){
                        modelTwo.removeAllItems();
                        modelTwo.enable();
                        modelTwo.addItem("Model");
                        modelTwo.addItem("Defender 2");
                        modelTwo.addItem("X-Tour 2");
                        modelTwo.addItem("Premier LTX");
                        modelTwo.addItem("CrossClimate 2");
                        modelTwo.addItem("Defender LTX");
                        modelTwo.addItem("X LT A/S");
                        modelTwo.addItem("LTX M/S 2");
                        modelTwo.addItem("LTX A/T 2");
                        modelTwo.addItem("Primacy MXM4");
                        modelTwo.addItem("Primacy LTX");
                        modelTwo.addItem("Primacy All Season");
                        modelTwo.addItem("Primacy HP");
                        modelTwo.addItem("Primacy Tour A/S");
                        modelTwo.addItem("Defender T+H");
                        modelTwo.addItem("Agilis CrossClimate");
                        modelTwo.addItem("Latitude Tour HP");
                        modelTwo.addItem("Energy Saver A/S");
                        modelTwo.addItem("Pilot Sport 4S");
                        modelTwo.addItem("Pilot Sport A/S 4");
                        modelTwo.addItem("Pilot Sport 4 SUV");
                        modelTwo.addItem("Pilot Sport 4");
                        modelTwo.addItem("Pilot Sport PS2");
                        modelTwo.addItem("Pilot Super Sport");
                        modelTwo.addItem("Pilot Sport Cup 2");
                        modelTwo.addItem("Latitude X-Ice Xi2");
                        modelTwo.addItem("Pilot Alpin PA4");
                        modelTwo.addItem("X-ICE Snow");
                        modelTwo.addItem("X-Ice Xi3");
                        modelTwo.addItem("Pilot Alpin 5");
                        modelTwo.addItem("Pilot Alpin 5 SUV");
                        modelTwo.addItem("Latitude Alpin");
                    }
                    else if(brandTwo.getSelectedItem().equals("Bridgestone")){
                        modelTwo.enable();
                        modelTwo.removeAllItems();
                        modelTwo.addItem("Model");
                        modelTwo.addItem("Turanza QuietTrack");
                        modelTwo.addItem("WeatherPeak");
                        modelTwo.addItem("Ecopia EP422+");
                        modelTwo.addItem("Alenza AS Ultra");
                        modelTwo.addItem("Dueler H/L Alenza+");
                        modelTwo.addItem("Dueler A/T Revo 3");
                        modelTwo.addItem("DriveGuard+");
                        modelTwo.addItem("Dueler H/T 684 II");
                        modelTwo.addItem("Dueler H/T 685");
                        modelTwo.addItem("Dueler H/P Sport AS");
                        modelTwo.addItem("Dueler H/P Sport");
                        modelTwo.addItem("Potenza RE980AS+");
                        modelTwo.addItem("Potenza Sport");
                        modelTwo.addItem("Potenza RE050A");
                        modelTwo.addItem("Potenza S007A RFT");
                        modelTwo.addItem("Potenza RE-71RS");
                        modelTwo.addItem("Blizzak LM005");
                        modelTwo.addItem("Blizzak WS90");
                        modelTwo.addItem("Blizzak DM-V2");
                    }

                }

                if (brandTwo.getSelectedItem().equals("BFG") || brandTwo.getSelectedItem().equals("Firestone")) {
                    if(activateCheckBox2.isSelected()){
                        couponTwo.setText("$110 OFF");
                        couponLabelTwo.setVisible(true);
                        if (installTwo.isSelected()) {
                            if (tpmsTwo.isSelected()) {
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if (disposal2.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 110.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));

                            } else if (!tpmsTwo.isSelected()) {
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                if (disposal2.isSelected()) {
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 110.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));
                            }

                        }
                    }

                    if(brandTwo.getSelectedItem().equals("BFG")){
                        modelTwo.enable();
                        modelTwo.removeAllItems();
                        modelTwo.addItem("Model");
                        modelTwo.addItem("G-Force COMP-2 A/S+");
                        modelTwo.addItem("Advantage Control");
                        modelTwo.addItem("Radial T/A");
                        modelTwo.addItem("Trail-Terrain T/A");
                        modelTwo.addItem("All-Terrain T/A KO2");
                        modelTwo.addItem("Mud-Terrain T/A KM3");
                        modelTwo.addItem("Advantage T/A Sport LT");
                    }
                    else if(brandTwo.getSelectedItem().equals("Firestone")){
                        modelTwo.enable();
                        modelTwo.removeAllItems();
                        modelTwo.addItem("Model");
                        modelTwo.addItem("Destination LE3");
                        modelTwo.addItem("Destination A/T 2");
                        modelTwo.addItem("Destination X/T");
                        modelTwo.addItem("FireHawk AS");
                        modelTwo.addItem("FireHawk Indy 500");
                        modelTwo.addItem("TransForce AT 2");
                        modelTwo.addItem("TransForce HT 2");
                        modelTwo.addItem("TransForce CV");
                    }


                }

            }
        });



        priceOne.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                        installOne.setSelected(true);
                        tpmsOne.setSelected(true);
                        disposal1.setSelected(true);
                        quantityOne.setValue(4);
                        Double sub_total;
                        Double price_one = Double.parseDouble(priceOne.getText());
                        Integer qty = (Integer) quantityOne.getValue();
                        sub_total = price_one;
                        if(installOne.isSelected()){
                            sub_total += 19.99;
                        }

                        if(tpmsOne.isSelected()){
                            sub_total += 2.99;
                        }

                        if(disposal1.isSelected()){
                            sub_total += 2.00;
                        }

                        sub_total = sub_total * qty;

                    if(activateCheckBox1.isSelected() && (brandOne.getSelectedItem().equals("Michelin") ||
                            brandOne.getSelectedItem().equals("Bridgestone"))){
                        sub_total -= 150.00;
                    }
                    if(activateCheckBox1.isSelected() && (brandOne.getSelectedItem().equals("BFG") ||
                            brandOne.getSelectedItem().equals("Firestone"))){
                        sub_total -= 110.00;

                    }
                        Double actual_total = sub_total * 0.06 + sub_total;

                        subTotal1.setText(df.format(sub_total));
                        actualTotal1.setText(df.format(actual_total));


                } catch (NumberFormatException numberFormatException) {

                    JOptionPane.showMessageDialog(priceOne,"Enter a valid price");
                    priceOne.setText("Price");
                    couponLabelOne.setVisible(false);
                    couponOne.setText(null);
                    actualTotal1.setText(null);
                    subTotal1.setText(null);
                    installOne.setSelected(false);
                    tpmsOne.setSelected(false);
                    disposal1.setSelected(false);
                    activateCheckBox1.setSelected(false);
                }


            }
        });

        priceTwo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    installTwo.setSelected(true);
                    tpmsTwo.setSelected(true);
                    disposal2.setSelected(true);
                    quantityTwo.setValue(4);
                    Double sub_total;
                    Double price_one = Double.parseDouble(priceTwo.getText());
                    Integer qty = (Integer) quantityTwo.getValue();
                    sub_total = price_one;
                    if(installTwo.isSelected()){
                        sub_total += 19.99;
                    }

                    if(tpmsTwo.isSelected()){
                        sub_total += 2.99;
                    }

                    if(disposal2.isSelected()){
                        sub_total += 2.00;
                    }

                    sub_total = sub_total * qty;

                    if(activateCheckBox2.isSelected() && (brandTwo.getSelectedItem().equals("Michelin") ||
                            brandTwo.getSelectedItem().equals("Bridgestone"))){
                        sub_total -= 150.00;
                    }
                    if(activateCheckBox2.isSelected() && (brandTwo.getSelectedItem().equals("BFG") ||
                            brandTwo.getSelectedItem().equals("Firestone"))){
                        sub_total -= 110.00;

                    }
                    Double actual_total = sub_total * 0.06 + sub_total;

                    subTotal2.setText(df.format(sub_total));
                    actualTotal2.setText(df.format(actual_total));


                } catch (NumberFormatException numberFormatException) {

                    JOptionPane.showMessageDialog(priceTwo,"Enter a valid price");
                    priceTwo.setText("Price");
                    couponLabelTwo.setVisible(false);
                    couponTwo.setText(null);
                    actualTotal2.setText(null);
                    subTotal2.setText(null);
                    installTwo.setSelected(false);
                    tpmsTwo.setSelected(false);
                    disposal2.setSelected(false);
                    activateCheckBox2.setSelected(false);
                }


            }
        });


        installOne.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    if(!priceOne.equals("Price")){
                        if(installOne.isSelected()) {
                            disposal1.setSelected(true);
                            tpmsOne.setSelected(true);

                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());
                            sub_total += 19.99;
                            if(tpmsOne.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal1.isSelected()){
                                sub_total += 2.00;
                            }
                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                        else if(!installOne.isSelected()){
                            activateCheckBox1.setSelected(false);
                            couponLabelOne.setVisible(false);
                            couponOne.setText(null);
                            disposal1.setSelected(false);
                            tpmsOne.setSelected(false);
                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                    }

                } catch (NumberFormatException numberFormatException) {
                    JOptionPane.showMessageDialog(installOne,"Enter Price");
                    installOne.setSelected(false);
                    tpmsOne.setSelected(false);
                    disposal1.setSelected(false);
                }

            }

        });

        installTwo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    if(!priceTwo.equals("Price")){
                        if(installTwo.isSelected()) {
                            disposal2.setSelected(true);
                            tpmsTwo.setSelected(true);

                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());
                            sub_total += 19.99;
                            if(tpmsTwo.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal2.isSelected()){
                                sub_total += 2.00;
                            }
                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                        else if(!installTwo.isSelected()){
                            activateCheckBox2.setSelected(false);
                            couponLabelTwo.setVisible(false);
                            couponTwo.setText(null);
                            disposal2.setSelected(false);
                            tpmsTwo.setSelected(false);
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                    }

                } catch (NumberFormatException numberFormatException) {
                    JOptionPane.showMessageDialog(installTwo,"Enter Price");
                    installTwo.setSelected(false);
                    tpmsTwo.setSelected(false);
                    disposal2.setSelected(false);
                }

            }

        });

        tpmsOne.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!installOne.isSelected()){
                    tpmsOne.setSelected(false);
                }else if(installOne.isSelected()){
                    if(tpmsOne.isSelected()){
                        Integer qty = (Integer) quantityOne.getValue();
                        Double sub_total = Double.parseDouble(priceOne.getText());
                        sub_total += 19.99;
                        sub_total += 2.99;
                        if(disposal1.isSelected()){
                            sub_total += 2.00;
                        }
                        sub_total = sub_total * qty;


                        if(activateCheckBox1.isSelected()){
                            if(brandOne.getSelectedItem().equals("Michelin") ||
                                    brandOne.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandOne.getSelectedItem().equals("BFG") ||
                                    brandOne.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }

                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal1.setText(df.format(sub_total));
                        actualTotal1.setText(df.format(actual_total));

                    }
                    else if(!tpmsOne.isSelected()){
                        Integer qty = (Integer) quantityOne.getValue();
                        Double sub_total = Double.parseDouble(priceOne.getText());
                        sub_total += 19.99;
                        if(disposal1.isSelected()){
                            sub_total += 2.00;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox1.isSelected()){
                            if(brandOne.getSelectedItem().equals("Michelin") ||
                                    brandOne.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandOne.getSelectedItem().equals("BFG") ||
                                    brandOne.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal1.setText(df.format(sub_total));
                        actualTotal1.setText(df.format(actual_total));
                    }
                }
            }
        });

        tpmsTwo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!installTwo.isSelected()){
                    tpmsTwo.setSelected(false);
                }else if(installTwo.isSelected()){
                    if(tpmsTwo.isSelected()){
                        Integer qty = (Integer) quantityTwo.getValue();
                        Double sub_total = Double.parseDouble(priceTwo.getText());
                        sub_total += 19.99;
                        sub_total += 2.99;
                        if(disposal2.isSelected()){
                            sub_total += 2.00;
                        }
                        sub_total = sub_total * qty;


                        if(activateCheckBox2.isSelected()){
                            if(brandTwo.getSelectedItem().equals("Michelin") ||
                                    brandTwo.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandTwo.getSelectedItem().equals("BFG") ||
                                    brandTwo.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }

                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal2.setText(df.format(sub_total));
                        actualTotal2.setText(df.format(actual_total));

                    }
                    else if(!tpmsTwo.isSelected()){
                        Integer qty = (Integer) quantityTwo.getValue();
                        Double sub_total = Double.parseDouble(priceTwo.getText());
                        sub_total += 19.99;
                        if(disposal2.isSelected()){
                            sub_total += 2.00;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox2.isSelected()){
                            if(brandTwo.getSelectedItem().equals("Michelin") ||
                                    brandTwo.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandTwo.getSelectedItem().equals("BFG") ||
                                    brandTwo.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal2.setText(df.format(sub_total));
                        actualTotal2.setText(df.format(actual_total));
                    }
                }
            }
        });

        disposal1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!installOne.isSelected()){
                    disposal1.setSelected(false);
                }else if(installOne.isSelected()) {
                    if (disposal1.isSelected()) {
                        Integer qty = (Integer) quantityOne.getValue();
                        Double sub_total = Double.parseDouble(priceOne.getText());
                        sub_total += 19.99;
                        sub_total += 2.00;
                        if (tpmsOne.isSelected()) {
                            sub_total += 2.99;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox1.isSelected()){
                            if(brandOne.getSelectedItem().equals("Michelin") ||
                                    brandOne.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandOne.getSelectedItem().equals("BFG") ||
                                    brandOne.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal1.setText(df.format(sub_total));
                        actualTotal1.setText(df.format(actual_total));

                    } else if (!disposal1.isSelected()) {
                        Integer qty = (Integer) quantityOne.getValue();
                        Double sub_total = Double.parseDouble(priceOne.getText());
                        sub_total += 19.99;
                        if (tpmsOne.isSelected()) {
                            sub_total += 2.99;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox1.isSelected()){
                            if(brandOne.getSelectedItem().equals("Michelin") ||
                                    brandOne.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandOne.getSelectedItem().equals("BFG") ||
                                    brandOne.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal1.setText(df.format(sub_total));
                        actualTotal1.setText(df.format(actual_total));
                    }
                }

            }
        });

        disposal2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(!installTwo.isSelected()){
                    disposal2.setSelected(false);
                }else if(installTwo.isSelected()) {
                    if (disposal2.isSelected()) {
                        Integer qty = (Integer) quantityTwo.getValue();
                        Double sub_total = Double.parseDouble(priceTwo.getText());
                        sub_total += 19.99;
                        sub_total += 2.00;
                        if (tpmsTwo.isSelected()) {
                            sub_total += 2.99;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox2.isSelected()){
                            if(brandTwo.getSelectedItem().equals("Michelin") ||
                                    brandTwo.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandTwo.getSelectedItem().equals("BFG") ||
                                    brandTwo.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal2.setText(df.format(sub_total));
                        actualTotal2.setText(df.format(actual_total));

                    } else if (!disposal2.isSelected()) {
                        Integer qty = (Integer) quantityTwo.getValue();
                        Double sub_total = Double.parseDouble(priceTwo.getText());
                        sub_total += 19.99;
                        if (tpmsTwo.isSelected()) {
                            sub_total += 2.99;
                        }
                        sub_total = sub_total * qty;
                        if(activateCheckBox2.isSelected()){
                            if(brandTwo.getSelectedItem().equals("Michelin") ||
                                    brandTwo.getSelectedItem().equals("Bridgestone")){
                                sub_total -= 150.00;
                            }
                            else if(brandTwo.getSelectedItem().equals("BFG") ||
                                    brandTwo.getSelectedItem().equals("Firestone")){
                                sub_total -= 110.00;
                            }

                        }
                        Double actual_total = sub_total * 0.06 + sub_total;
                        subTotal2.setText(df.format(sub_total));
                        actualTotal2.setText(df.format(actual_total));
                    }
                }

            }
        });

        activateCheckBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(brandOne.getSelectedItem().equals("Select Brand")){
                    activateCheckBox1.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox1,"Select a Brand");
                }
                if((Integer) quantityOne.getValue() < 4){
                    activateCheckBox1.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox1,"Quantity must be 4 or higher");
                }
                if(activateCheckBox1.isSelected() && (Integer) quantityOne.getValue() >= 4){
                    if(installOne.isSelected()){
                        if(brandOne.getSelectedItem().equals("Michelin") || brandOne.getSelectedItem().equals("Bridgestone")){
                            couponOne.setText("$150 OFF");
                            couponLabelOne.setVisible(true);
                            if(tpmsOne.isSelected()){
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if(disposal1.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));

                            }
                            else if(!tpmsOne.isSelected()){
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                if(disposal1.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));
                            }
                        }
                        if(installOne.isSelected()) {
                            if (brandOne.getSelectedItem().equals("Firestone") || brandOne.getSelectedItem().equals("BFG")) {
                                couponOne.setText("$110 OFF");
                                couponLabelOne.setVisible(true);
                                if (tpmsOne.isSelected()) {
                                    Integer qty = (Integer) quantityOne.getValue();
                                    Double sub_total = Double.parseDouble(priceOne.getText());
                                    sub_total += 19.99;
                                    sub_total += 2.99;
                                    if (disposal1.isSelected()) {
                                        sub_total += 2.00;
                                    }
                                    sub_total = sub_total * qty - 110.00;
                                    Double actual_total = sub_total * 0.06 + sub_total;
                                    subTotal1.setText(df.format(sub_total));
                                    actualTotal1.setText(df.format(actual_total));

                                } else if (!tpmsOne.isSelected()) {
                                    Integer qty = (Integer) quantityOne.getValue();
                                    Double sub_total = Double.parseDouble(priceOne.getText());
                                    sub_total += 19.99;
                                    if (disposal1.isSelected()) {
                                        sub_total += 2.00;
                                    }
                                    sub_total = sub_total * qty - 110.00;
                                    Double actual_total = sub_total * 0.06 + sub_total;
                                    subTotal1.setText(df.format(sub_total));
                                    actualTotal1.setText(df.format(actual_total));
                                }
                            }
                        }
                    }

                }

                if(!activateCheckBox1.isSelected()){
                    couponOne.setText(null);
                    couponLabelOne.setVisible(false);
                    if(installOne.isSelected()){
                            if(tpmsOne.isSelected()){
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if(disposal1.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));

                            }
                            else if(!tpmsOne.isSelected()){
                                Integer qty = (Integer) quantityOne.getValue();
                                Double sub_total = Double.parseDouble(priceOne.getText());
                                sub_total += 19.99;
                                if(disposal1.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal1.setText(df.format(sub_total));
                                actualTotal1.setText(df.format(actual_total));
                            }

                    }

                }

                if(!installOne.isSelected()){
                    activateCheckBox1.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox1,"Installation Required");

                }


            }
        });

        activateCheckBox2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(brandTwo.getSelectedItem().equals("Select Brand")){
                    activateCheckBox2.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox2,"Select a Brand");
                }
                if((Integer) quantityTwo.getValue() < 4){
                    activateCheckBox2.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox2,"Quantity must be 4 or higher");
                }
                if(activateCheckBox2.isSelected() && (Integer) quantityTwo.getValue() >= 4){
                    if(installTwo.isSelected()){
                        if(brandTwo.getSelectedItem().equals("Michelin") || brandTwo.getSelectedItem().equals("Bridgestone")){
                            couponTwo.setText("$150 OFF");
                            couponLabelTwo.setVisible(true);
                            if(tpmsTwo.isSelected()){
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                sub_total += 2.99;
                                if(disposal2.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));

                            }
                            else if(!tpmsTwo.isSelected()){
                                Integer qty = (Integer) quantityTwo.getValue();
                                Double sub_total = Double.parseDouble(priceTwo.getText());
                                sub_total += 19.99;
                                if(disposal2.isSelected()){
                                    sub_total += 2.00;
                                }
                                sub_total = sub_total * qty - 150.00;
                                Double actual_total = sub_total * 0.06 + sub_total;
                                subTotal2.setText(df.format(sub_total));
                                actualTotal2.setText(df.format(actual_total));
                            }
                        }
                        if(installTwo.isSelected()) {
                            if (brandTwo.getSelectedItem().equals("Firestone") || brandTwo.getSelectedItem().equals("BFG")) {
                                couponTwo.setText("$110 OFF");
                                couponLabelTwo.setVisible(true);
                                if (tpmsTwo.isSelected()) {
                                    Integer qty = (Integer) quantityTwo.getValue();
                                    Double sub_total = Double.parseDouble(priceTwo.getText());
                                    sub_total += 19.99;
                                    sub_total += 2.99;
                                    if (disposal2.isSelected()) {
                                        sub_total += 2.00;
                                    }
                                    sub_total = sub_total * qty - 110.00;
                                    Double actual_total = sub_total * 0.06 + sub_total;
                                    subTotal2.setText(df.format(sub_total));
                                    actualTotal2.setText(df.format(actual_total));

                                } else if (!tpmsTwo.isSelected()) {
                                    Integer qty = (Integer) quantityTwo.getValue();
                                    Double sub_total = Double.parseDouble(priceTwo.getText());
                                    sub_total += 19.99;
                                    if (disposal2.isSelected()) {
                                        sub_total += 2.00;
                                    }
                                    sub_total = sub_total * qty - 110.00;
                                    Double actual_total = sub_total * 0.06 + sub_total;
                                    subTotal2.setText(df.format(sub_total));
                                    actualTotal2.setText(df.format(actual_total));
                                }
                            }
                        }
                    }

                }

                if(!activateCheckBox2.isSelected()){
                    couponTwo.setText(null);
                    couponLabelTwo.setVisible(false);
                    if(installTwo.isSelected()){
                        if(tpmsTwo.isSelected()){
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());
                            sub_total += 19.99;
                            sub_total += 2.99;
                            if(disposal2.isSelected()){
                                sub_total += 2.00;
                            }
                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));

                        }
                        else if(!tpmsTwo.isSelected()){
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());
                            sub_total += 19.99;
                            if(disposal2.isSelected()){
                                sub_total += 2.00;
                            }
                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }

                    }

                }

                if(!installTwo.isSelected()){
                    activateCheckBox2.setSelected(false);
                    JOptionPane.showMessageDialog(activateCheckBox2,"Installation Required");

                }


            }
        });

        quantityOne.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

                if((Integer) quantityOne.getValue() < 1){
                    JOptionPane.showMessageDialog(quantityOne,"Quantity cannot be 0");
                    quantityOne.setValue(1);
                }

                if ((Integer) quantityOne.getValue() >= 4){
                    if(!priceOne.equals("Price")){
                        if(installOne.isSelected()) {
                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());
                            sub_total += 19.99;
                            if(tpmsOne.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal1.isSelected()){
                                sub_total += 2.00;
                            }

                            sub_total = sub_total * qty;

                            if(activateCheckBox1.isSelected()){
                                if(brandOne.getSelectedItem().equals("Michelin") ||
                                        brandOne.getSelectedItem().equals("Bridgestone")){
                                    sub_total -= 150.00;
                                }
                                else if(brandOne.getSelectedItem().equals("BFG") ||
                                        brandOne.getSelectedItem().equals("Firestone")){
                                    sub_total -= 110.00;
                                }

                            }
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                        else if(!installOne.isSelected()){
                            activateCheckBox1.setSelected(false);
                            disposal1.setSelected(false);
                            tpmsOne.setSelected(false);
                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                    }
                }
                else if ((Integer) quantityOne.getValue() < 4){
                    activateCheckBox1.setSelected(false);
                    couponOne.setText(null);
                    couponLabelOne.setVisible(false);
                    if(!priceOne.equals("Price")){
                        if(installOne.isSelected()) {

                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());
                            sub_total += 19.99;
                            if(tpmsOne.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal1.isSelected()){
                                sub_total += 2.00;
                            }

                            sub_total = sub_total * qty;

                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                        else if(!installOne.isSelected()){
                            disposal1.setSelected(false);
                            tpmsOne.setSelected(false);
                            Integer qty = (Integer) quantityOne.getValue();
                            Double sub_total = Double.parseDouble(priceOne.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal1.setText(df.format(sub_total));
                            actualTotal1.setText(df.format(actual_total));
                        }
                    }

                }
            }
        });

        quantityTwo.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {

                if((Integer) quantityTwo.getValue() < 1){
                    JOptionPane.showMessageDialog(quantityTwo,"Quantity cannot be 0");
                    quantityTwo.setValue(1);
                }

                if ((Integer) quantityTwo.getValue() >= 4){
                    if(!priceTwo.equals("Price")){
                        if(installTwo.isSelected()) {
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());
                            sub_total += 19.99;
                            if(tpmsTwo.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal2.isSelected()){
                                sub_total += 2.00;
                            }

                            sub_total = sub_total * qty;

                            if(activateCheckBox2.isSelected()){
                                if(brandTwo.getSelectedItem().equals("Michelin") ||
                                        brandTwo.getSelectedItem().equals("Bridgestone")){
                                    sub_total -= 150.00;
                                }
                                else if(brandTwo.getSelectedItem().equals("BFG") ||
                                        brandTwo.getSelectedItem().equals("Firestone")){
                                    sub_total -= 110.00;
                                }

                            }
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                        else if(!installTwo.isSelected()){
                            activateCheckBox2.setSelected(false);
                            disposal2.setSelected(false);
                            tpmsTwo.setSelected(false);
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                    }
                }
                else if ((Integer) quantityTwo.getValue() < 4){
                    activateCheckBox2.setSelected(false);
                    couponTwo.setText(null);
                    couponLabelTwo.setVisible(false);
                    if(!priceTwo.equals("Price")){
                        if(installTwo.isSelected()) {

                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());
                            sub_total += 19.99;
                            if(tpmsTwo.isSelected()){
                                sub_total += 2.99;
                            }
                            if(disposal2.isSelected()){
                                sub_total += 2.00;
                            }

                            sub_total = sub_total * qty;

                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                        else if(!installTwo.isSelected()){
                            disposal2.setSelected(false);
                            tpmsTwo.setSelected(false);
                            Integer qty = (Integer) quantityTwo.getValue();
                            Double sub_total = Double.parseDouble(priceTwo.getText());

                            sub_total = sub_total * qty;
                            Double actual_total = sub_total * 0.06 + sub_total;
                            subTotal2.setText(df.format(sub_total));
                            actualTotal2.setText(df.format(actual_total));
                        }
                    }

                }
            }
        });
    }



}












