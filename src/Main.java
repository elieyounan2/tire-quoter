
import com.formdev.flatlaf.intellijthemes.*;


import javax.swing.*;

public class Main extends TireQuoter{


    public static void main(String[] args) {

        //FlatIntelliJLaf.registerCustomDefaultsSource("style");
        FlatArcIJTheme.setup();
        TireQuoter frame = new TireQuoter();
        frame.setTitle("Tire Quoter");
        frame.setContentPane(frame.Panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(frame.getWidth() + 200, frame.getHeight());
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
